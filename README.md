# A propos #


### Ce dépôt vous permet de gérer une carte horloge RTC DS1302 connectée à une Raspberry Pi ###

* Cette application est destinée à piloter une carte horloge matérielle spécifique pour une Raspberry Pi.
* Version 3 (voir les 2 liens ci-dessous)
* [Article en français](http://www.framboise314.fr/un-module-rtc-a-base-de-ds1302-pour-le-raspberry-pi/)
* [Artcile en anglais](http://www.hobbytronics.co.uk/tutorials-code/raspberry-pi-tutorials/raspberry-pi-real-time-clock)

### Installer le matériel ###

* Le matériel  est constitué d'une carte horloge matérielle spécifique connectée par 5 fils comme décrit dans les liens ci-dessus.
* Elle n'est pas compatible avec la liaison I2C
* Faites très attention car si la carte horloge en elle même supporte le 5V, **les pins du connecteur GPIO P1 ne supportent que le 3,3V maximum.**
* J'ai dévelopé une carte spécifique Téléinfo, OneWire et RTC avec cette carte horloge

### Installer ce logiciel ###

* Télécharger ou clonez ce dépôt
* Configuration: Raspbian, ce logiciel
* Dependances: nécessite une carte horloge correctement connectée à une Raspberry Pi. Voyez les liens ci-dessus qui sont très explicites
* A éxécuter en tant que root, lancez directement rtc-piv3 -h pour lister les paramètres possibles
* NON compatible avec hwclock ni adjtimex: TODO mais beaucoup de travail

### Contributions ###

* N'hésitez pas à proposer une demande d'ajout ou un signalement de bug
* Pour recompiler le logiciel faire: 
```
#!bash

cc rtc-piv3.c -o rtc-piv3 -g
```
* Pour debugger avec gdb

```
#!bash

gdb --args rtc-piv3 -h
```
* Pour obtenir une version sans symboles de debogage

```
#!bash

cc rtc-piv3.c -o rtc-piv3
```
## Utilisation ##
Exemple
* liste des commandes

```
#!bash

$./rtc-piv3 -h
Usage: ./rtc-piv3
-t CCYYMMDDHHMMSS defines the rtc date and time
-d display rtc status date, time and sram
-s set the system date and time from rtc 
-u updates rtc clock if it is older than system clock, or updates rtc clock if it is older than rtc clock
-w 0123456789ABCDEF0123456789ABCD 31 bytes long hexadecimal string to save in rtc's sram
-m returns the rtc sram content as a 31 bytes long hexadecimal string 
-v verbose display acknowledgement for actions
-h help
See for more subtle approaches http://www.tldp.org/HOWTO/Clock-2.htmlmer. 
$

```
* Affichage de l'état de l'horloge RTC

```
#!bash

$./rtc-piv3 -d
Etat actuel de l'horloge RTC DS1302
Date-Heure RTC à 2014-06-25 10:39:45

RTC sram read bytes: 01 23 45 67 89 AB CD EF 01 23 45 67 89 AB CD EF 01 23 45 67 89 AB CD EF 01 23 45 67 89 AB CD
$
```
* Modifier la date et l'heure de l'horloge

```
#!bash

$./rtc-piv3 -t 20140531124525
$./rtc-piv3 -d
Etat actuel de l'horloge RTC DS1302
Date-Heure RTC à 2014-05-31 12:45:45

RTC sram read bytes: 01 23 45 67 89 AB CD EF 01 23 45 67 89 AB CD EF 01 23 45 67 89 AB CD EF 01 23 45 67 89 AB CD
$#vérifions que la date système n'as pas été changée
$date
mercredi 25 juin 2014, 10:43:07 (UTC+0200)

```

* Modifier le contenu de la SRAM

```
#!bash

$./rtc-piv3 -v -w FEDCBA9876543210FEDCBA9876543210FEDCBA9876543210FEDCBA9876543201 
RTC sram bytes written: FE DC BA 98 76 54 32 10 FE DC BA 98 76 54 32 10 FE DC BA 98 76 54 32 10 FE DC BA 98 76 54 32 
$./rtc-piv3 -w FEDCBA9876543210FEDCBA9876543210FEDCBA9876543210FEDCBA9876543201
$
```

*Relire le contenu de la SRAM

```
#!bash

$./rtc-piv3 -v -m
RTC sram read bytes: FE DC BA 98 76 54 32 10 FE DC BA 98 76 54 32 10 FE DC BA 98 76 54 32 10 FE DC BA 98 76 54 32 
FEDCBA9876543210FEDCBA9876543210FEDCBA9876543210FEDCBA98765432
$./rtc-piv3 -m
FEDCBA9876543210FEDCBA9876543210FEDCBA9876543210FEDCBA98765432

```
* Synchroniser les 2 horloges sur la plus à l'heure


```
#!bash

 $./rtc-piv3 -v -u
Date-Heure système actuelles à 2014-06-25 12:14:34

Différence d'heures entre les horloges incohérentes: ( 2000 s <= rtc - system ) ou ( system - rtc <= 2 s )
$
```



### Qui suis-je? ###

* Repo owner: asez73
/* This software is published under the license described in file copyright.md
 * This original software was found without any mentionned license at 
 *   http://www.hobbytronics.co.uk/tutorials-code/raspberry-pi-tutorials/raspberry-pi-real-time-clock
 * Furthermore an improved version was found at 
 *   http://www.framboise314.fr/avant-lheure-cest-pas-lheure-apres-lheure-offrez-une-horloge-temps-reel-rtc-a-votre-raspberry-pi/
 *   Droits d’auteur et reproduction :
 *      Toute production éditoriale de framboise314.fr est en «Creative Commons» BY-NC-SA 3.0 France : 
 *      Attribution – Pas d’Utilisation Commerciale – Partage dans les Mêmes Conditions 3.0.
 *      [ http://creativecommons.org/licenses/by-nc-sa/3.0/fr/ ]
*/
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <time.h>
#include <string.h>
#include <errno.h>
#include <sys/mman.h>
#include <sys/time.h>
#include <getopt.h>   
                            
#define GPIO_ADD    0x20200000L // physical address of I/O peripherals on the ARM processor
#define GPIO_SEL1   1           // Offset of SEL register for GP17 & GP18 into GPIO bank   
#define GPIO_SEL2   2           // Offset of SEL register for GP21 into GPIO bank  
#define GPIO_SET    7           // Offset of PIN HIGH register into GPIO bank  
#define GPIO_CLR    10          // Offset of PIN LOW register into GPIO bank  
#define GPIO_INP    13          // Offset of PIN INPUT value register into GPIO bank  
#define PAGE_SIZE   4096        
#define BLOCK_SIZE  PAGE_SIZE

/* RTC Chip register definitions */
#define SEC_WRITE    0x80
#define MIN_WRITE    0x82
#define HOUR_WRITE   0x84
#define DATE_WRITE   0x86
#define MONTH_WRITE  0x88
#define YEAR_WRITE   0x8C
#define SEC_READ     0x81
#define MIN_READ     0x83
#define HOUR_READ    0x85
#define DATE_READ    0x87
#define MONTH_READ   0x89
#define YEAR_READ    0x8D
#define WR_ENABLE    0x8E

/* RTC chip registers for access to 31 SRAM bytes */
#define SRAM_WRITE   0xC0
#define SRAM_READ    0xC1

/* RTC chip registers for ram burst mode  */
#define RAM_BURST_WRITE 0xBE
#define RAM_BURST_READ  0xBF

int  mem_fd     = 0;
char *gpio_mmap = NULL;
char *gpio_ram  = NULL;
volatile unsigned int *gpio = NULL;

/* These 'defines' map the peripheral pin functions to our circuits DS1302 pins */
/* See DS1302 datasheet REV: 110805, and Broadcom BCM2835-ARM-Peripherals.pdf 6/2/2012 */
#define IO_INPUT    *(gpio+GPIO_SEL1) &= 0xF8FFFFFFL
#define IO_OUTPUT   *(gpio+GPIO_SEL1) &= 0xF8FFFFFFL; *(gpio+GPIO_SEL1) |= 0x01000000L 
#define CE_OUTPUT   *(gpio+GPIO_SEL1) &= 0xFF1FFFFFL; *(gpio+GPIO_SEL1) |= 0x00200000L
#define IO_HIGH     *(gpio+GPIO_SET) = 0x00040000L
#define IO_LOW      *(gpio+GPIO_CLR) = 0x00040000L
#define CE_HIGH     *(gpio+GPIO_SET) = 0x00020000L  
#define CE_LOW      *(gpio+GPIO_CLR) = 0x00020000L
#define IO_LEVEL    *(gpio+GPIO_INP) & 0x00040000L

/* depending upon V1 or v2 PCB */

/* changements pour version 2 de la Raspberry Pi */
#define SCLK_OUTPUT_v2 *(gpio+GPIO_SEL2) &= 0xFF1FFFFFL; *(gpio+GPIO_SEL2) |= 0x00200000L
#define SCLK_HIGH_v2 *(gpio+GPIO_SET) = 0x08000000L
#define SCLK_LOW_v2 *(gpio+GPIO_CLR) = 0x08000000L
/* changements pour version 1 de la Raspberry Pi */
#define SCLK_OUTPUT_v1 *(gpio+GPIO_SEL2) &= 0xFFFFFFC7L; *(gpio+GPIO_SEL2) |= 0x00000008L
#define SCLK_HIGH_v1   *(gpio+GPIO_SET) = 0x00200000L
#define SCLK_LOW_v1    *(gpio+GPIO_CLR) = 0x00200000L

int revision=-1; /* Raspberry Pi board revision extracted from /proc/cpuinfo */
int rev2=0; /* non zero if v2 board */
int rev1=0; /* non zero if v1 board */

/* verbose mode to display status and actions in detail */
int verbose_mode=0;

int board_revision() {
   FILE *f = fopen("/proc/cpuinfo", "r");
   if (!f) {
      printf("Impossible de déterminer la version de la Raspberry Pi: fichier /proc/cpuinfo impossible à ouvrir\n");
      exit( -1);
   }
   char line[256];
   int revision=-1;
   while (fgets(line, 256, f)) {
      if (strncmp(line, "Revision", 8) == 0) {
         char revision_string[4 + 1];
         revision = atoi(strcpy(revision_string, strchr(line, ':') + 2));
      }
   }
   fclose(f);
   rev2=(revision>3) && (revision<=15);
   rev1=(revision<=3) && (revision>=0);
   if (!rev2 && !rev1) {
     printf("Raspberry Pi board revision %d\n see details at http://elinux.org/RPi_HardwareHistory as v2 boards have a revision number > 3 and <= 15",revision);
     printf("Unknow board revision (neither v1 boards, nor v2 boards) OR might have been overvoltaged.....\n");
     exit(-1);
   }
   if (verbose_mode)
     printf("Raspberry Pi revision %d considered as v%s\n", revision,  ((rev2) ? "2" : ((rev1) ? "1" : "? inconnue"))); 
   return revision;
}

void setup_io(){
   /* open /dev/mem to get acess to physical ram */
   if ((mem_fd = open("/dev/mem", O_RDWR|O_SYNC) ) < 0) {
      printf("can't open /dev/mem. Did you run the program with administrator rights?\n");
      exit (-1);
   }
   /* Allocate a block of virtual RAM in our application's address space */
   if ((gpio_ram = malloc(BLOCK_SIZE + (PAGE_SIZE-1))) == NULL) {
      printf("allocation error \n");
      exit (-1);
   }
   /* Make sure the pointer is on 4K boundary */
   if ((unsigned long)gpio_ram % PAGE_SIZE)
     gpio_ram += PAGE_SIZE - ((unsigned long)gpio_ram % PAGE_SIZE);
   /* Now map the physical addresses of the peripheral control registers 
      into our address space */
   gpio_mmap = (unsigned char *)mmap(
      (caddr_t)gpio_ram,
      BLOCK_SIZE,
      PROT_READ|PROT_WRITE,
      MAP_SHARED|MAP_FIXED,
      mem_fd,
      GPIO_ADD
   );
   if ((long)gpio_mmap < 0) {
      printf("unable to map the memory. Did you run the program with administrator rights?\n");
      exit (-1);
   }
   /* Always use a volatile pointer to hardware registers */
   gpio = (volatile unsigned *)gpio_mmap;
   /* Now we have access to the hardware reigsters we can start twiddling I/O pins */
   /* Switch GPIO 0, 1 and 2 to output mode */
   if (rev2) { SCLK_OUTPUT_v2; }
   else { SCLK_OUTPUT_v1; }
   IO_OUTPUT;
   CE_OUTPUT;
   /* Set the SCLK, IO and CE pins to default (low) */
   if (rev2) { SCLK_LOW_v2; }
   else { SCLK_LOW_v1; }
   IO_LOW;
   CE_LOW;
   /* Short delay to allow the I/O lines to settle. */
   usleep(2);
}
void hand_shake_clk() {
  usleep(2);
  if (rev2) { SCLK_HIGH_v2; }
  else { SCLK_HIGH_v1; }
  usleep(2);
  if (rev2) { SCLK_LOW_v2; }
  else { SCLK_LOW_v1; }
  usleep(2);     
}

void write_byte(unsigned char val) {
   int lp;
   for (lp=0; lp<8; lp++) {
      if (val & 1) IO_HIGH;
      else IO_LOW;
      val >>= 1; 
      hand_shake_clk();
   }
}

unsigned char read_byte() {
   /* start reading answer */
   unsigned char val= 0;
   int lp;

   IO_INPUT; 
   for (lp=0; lp<8; lp++) {
      usleep(2);
      val >>= 1;
      if (IO_LEVEL) val |= 0x80;
      else val &= 0x7F;         
      hand_shake_clk();
   }
   /* Set the I/O pin back to it's default, output low. */
   IO_LOW;
   IO_OUTPUT;
   /* Set the CE pin back to it's default, low */
   CE_LOW;
   /* Short delay to allow the I/O lines to settle. */
   usleep(2);     
   return val;
}

unsigned char read_rtc( unsigned char address ){
   unsigned char val;

   /* Check LSB is set */
   if ( !(address & 1 ) ) {
      printf("Incorrect read address specified - LSB must be set.\n");
      exit (-1);
   }
   /* Check address range is valid */
   if (!(( (address >= 0x80) && (address <= 0x90)) || ((address>=0xC0) && (address<=0xFD)) )) {
      printf("Incorrect read address specified %d - It must be in the range 0x81..0x91\n", (unsigned int) address);
      exit (-1);
   }
   CE_HIGH;
   usleep(2);
   write_byte(address); /* write addresse to be read */
   return read_byte(); /* read byte answered */
}

void write_rtc( unsigned char address, unsigned char val_to_write ){
   unsigned char val;
   int lp;

   /* Check LSB is clear */
   if ( address & 1 ) {
      printf("Incorrect write address specified %d - LSB must be cleared.\n", (unsigned int) address);
      exit (-1);
   }
   /* Check address range is valid */
   if (!(( (address >= 0x80) && (address <= 0x90)) || ((address>=0xC0) && (address<=0xFD)) )) {
      printf("Incorrect write address specified %d - It must be in the range 0x80..0x90 (for time and date) or 0xC0..0xFC (for sram address)\n",
             (unsigned int) address);
      exit (-1);
   }
   CE_HIGH;
   usleep(2);
   write_byte(address);
   write_byte(val_to_write);
   /* Set the I/O pin back to it's default, output low. */
   IO_LOW;
   /* Set the CE pin back to it's default, low */
   CE_LOW;
   /* Short delay to allow the I/O lines to settle. */
   usleep(2);     
}

unsigned char ds1302_sram_copy[32] ;

void ds1302_read_sram() { /* copy sram from rtc to ram buffer */
  int i;

  if (verbose_mode)
    printf("RTC sram read bytes: ");
  for (i=0; i<31; i++) {
    ds1302_sram_copy[i] = read_rtc((i << 1) + SRAM_READ);
    if (verbose_mode)
      printf("%.2X ", ds1302_sram_copy[i]);
  }
  if (verbose_mode)
    printf("\n");
}
unsigned char hex_to_char(unsigned char *p) {
  int v1,v2;
  
  v1=(int) *p;
  if ((v1>='0') && (v1<='9')) v1=v1-'0';
  else
    if ((v1>='A') && (v1<='F')) v1=v1+10-'A';
    else
      if ((v1>='a') && (v1<='f')) v1=v1+10-'a';
  v2=(int) p[1];
  if ((v2>='0') && (v2<='9')) v2=v2-'0';
  else
    if ((v2>='A') && (v2<='F')) v2=v2+10-'A';
    else
      if ((v2>='a') && (v2<='f')) v2=v2+10-'a';
  return (unsigned char) (v1*16+v2);
}

void ds1302_write_sram(unsigned char *p) { /* copy hexadecimal coded buffer to rtc */
  unsigned int i;
  unsigned char c;
  unsigned char address;

  if (verbose_mode)
    printf("RTC sram bytes written: ");
  address=SRAM_WRITE;
  for (i = 0; i < 31; i++) {
    c=hex_to_char(p);
    write_rtc(address, c);
    if (verbose_mode)
      printf("%.2X ", c);
    p+=2;
    address+=2;
    ds1302_sram_copy[i]= c;
  }
  if (verbose_mode)
    printf("\n");
}

time_t read_rtc_time() {
  struct tm time_detailled;
  int year, month, day, hour, minute, second;

  second = read_rtc(SEC_READ) & 0x7F;
  time_detailled.tm_sec =((second >> 4) * 10) + (second & 0x0F); /* 0-59 */
  minute = read_rtc(MIN_READ) & 0x7F;
  time_detailled.tm_min =((minute >> 4) * 10) + (minute & 0x0F); /* 0-59 */
  hour = read_rtc(HOUR_READ) & 0x3F;
  time_detailled.tm_hour=((hour >> 4) * 10) + (hour & 0x0F); /* 0-23 */
  day = read_rtc(DATE_READ) & 0x3F;
  time_detailled.tm_mday=((day >> 4) * 10) + (day & 0x0F);  /* 1-31 */
  month = read_rtc(MONTH_READ) & 0x1F;
  time_detailled.tm_mon =((month >> 4) * 10) + (month & 0x0F) - 1; /* 1-12 shifted to 0-11 */
  year = read_rtc(YEAR_READ) & 0xFF;   
  time_detailled.tm_year=((year >> 4) * 10) + (year & 0x0F) + 2000 - 1900; /* This is a post 2000 year software, so no more hassle */
  time_detailled.tm_wday = 0;           /* not used */
  time_detailled.tm_yday = 0;           /* not used */
  time_detailled.tm_isdst = -1;         /* determine daylight saving time from the system */
  /* convert to epoch */
  return(mktime(&time_detailled)); /* correct exclusively if isdst was*/
}

int set_rtc_time(time_t epoch_time) {
   int year, month, day, hour, minute, second;
   struct tm *time_local;

   if (epoch_time!=0) {
     time_local = localtime(&epoch_time);
     year       = (*time_local).tm_year+1900;
     month      = (*time_local).tm_mon+1;
     day        = (*time_local).tm_mday;
     hour       = (*time_local).tm_hour;
     minute     = (*time_local).tm_min;
     second     = (*time_local).tm_sec;
     /* Validate that the input date and time is basically sensible */
     if ( (year < 2013) || (year > 2099) || (month < 1) || (month > 12) ||
          (day < 1) || (day>31) || (hour < 0) || (hour > 23) || (minute < 0) ||
          (minute > 59) || (second < 0) || (second > 59) ) {
        printf("Incorrect date and time specified.\nRun as:\nrtc-pi\nor\nrtc-pi CCYYMMDDHHMMSS\n");
        return (-1);
     }
     else {
       /* Got valid input - now write it to the RTC */
       /* The RTC expects the values to be written in packed BCD format */
       /* Mettre le bit write protect à 0 */
       write_rtc(WR_ENABLE, 0); 
       write_rtc(SEC_WRITE, ( (second/10) << 4) | ( second % 10) );
       write_rtc(MIN_WRITE, ( (minute/10) << 4) | ( minute % 10) );
       write_rtc(HOUR_WRITE, ( (hour/10) << 4) | ( hour % 10) );
       write_rtc(DATE_WRITE, ( (day/10) << 4) | ( day % 10) );
       write_rtc(MONTH_WRITE, ( (month/10) << 4) | ( month % 10) );
       write_rtc(YEAR_WRITE, ( ((year-2000)/10) << 4) | (year % 10) );   
       if (verbose_mode) 
         printf("Date-Heure RTC positionnée à %d-%d-%d %d:%d:%d\n", year, month, day, hour, minute, second);
     }
     return 0;
  }
  else
    return(-1);
}

time_t read_system_time() {
   /* read the system time and returns it in different variables in the usual local notation */
   time_t now;
   char s[40];
   size_t i;

   now = time(NULL);
   if (verbose_mode) {
     i = strftime(s,sizeof(s),"%Y-%m-%d %H:%M:%S\n", (localtime(&now)));
     printf("Date-Heure système actuelles à %s\n", s);
   }  
   return(now);
}

void print_rtc_status() {
   int i;
   time_t rtc_time, sys_time;
   double ecart;
   char s[40];

   i = verbose_mode;
   verbose_mode = 1;
   printf("Etat actuel de l'horloge RTC DS1302\n");
   sys_time=read_system_time();
   rtc_time = read_rtc_time( );
   i = strftime(s,sizeof(s),"%Y-%m-%d %H:%M:%S\n", (localtime(&rtc_time)));
   printf("Date-Heure RTC à %s\n", s);
   ecart=difftime(sys_time,rtc_time);
   if (abs(ecart)>1) {
	   if (ecart>0) {
	   	printf("L'horloge RTC est en retard de %'.2f secondes sur l'heure système\n",ecart);
	   }
	   else {
	   	printf("L'horloge RTC est en avance de %'.2f secondes sur l'heure système\n", ecart);
	   }
   }
   else printf("Pas d'écart d'heure mesurable entre la RTC et l'heure système\n");
   ds1302_read_sram();
   verbose_mode = i;
}

void set_system_time(time_t epoch_time) {
   struct timeval time_format;
   char s[40];
   size_t i;

   time_format.tv_sec = epoch_time;
   time_format.tv_usec = 0;
   /* Check that the change was successful */
   if ( settimeofday(&time_format,NULL) < 0 ) {  
      printf("Unable to change the system time. Did you run the program as an administrator?\n");
      printf("The operation returned the error message \"%s\"\n", strerror( errno ) );
      exit (-1);
   }
   else
     if (verbose_mode) {
       i = strftime(s,sizeof(s),"%Y-%m-%d %H:%M:%S\n", (localtime(&epoch_time)));
       printf("Date-Heure système modifiés à %s\n", s);
     }  
 }

void set_system_time_from_rtc() {
   time_t rtc_epoch;

   rtc_epoch=read_rtc_time();
   if (rtc_epoch>2014*365.25*24*3600) 
     set_system_time(rtc_epoch);
  else 
   if (verbose_mode)
     printf("RTC time is incorrect, nothing done\n");
}

void sync_oldest_clock_to_newest(){ /* As its name suggests, however fool proof guard is considered */
  time_t rtc_time, sys_time;

  rtc_time= read_rtc_time();
  sys_time= read_system_time();
  if (difftime(rtc_time, sys_time)>2000) { /* in this case ntpd will not update system time, thus we can safely update system time from rtc time */
    set_system_time(rtc_time);
  }
  else {
    if (difftime(sys_time, rtc_time)>2) { /* this means that the rtc clock is slower than real time, so we can update safely */
      set_rtc_time(sys_time);
    }
    else
      if (verbose_mode)
        printf("Différence d'heures entre les horloges incohérentes: ( 2000 s <= rtc - system ) ou ( system - rtc <= 2 s )\n");
  }
}

time_t stringio_to_epoch(char *line) {
  struct tm time_detailled;

  if (6==sscanf(line,"%4d%2d%2d%2d%2d%2d", &time_detailled.tm_year, &time_detailled.tm_mon, &time_detailled.tm_mday, 
                                           &time_detailled.tm_hour, &time_detailled.tm_min, &time_detailled.tm_sec)) {
    time_detailled.tm_mon = time_detailled.tm_mon - 1;    /* 0 à 11 in tm while 1-12 in input */ 
    time_detailled.tm_year = time_detailled.tm_year  - 1900; /* 0 is 1900 in tm while 1900-2100 in input */
    time_detailled.tm_wday = 0;           /* not used */
    time_detailled.tm_yday = 0;           /* not used */
    time_detailled.tm_isdst = -1;         /* determine daylight saving time from the system */
    return(mktime(&time_detailled)); /* correct exclusively if isdst was*/
  }
  else 
    return(0);
}


int main(int argc, char **argv){ 
   unsigned int c;
   int opt, i;
   char *p;

   /* Set up gpi pointer for direct register access */
   revision=board_revision(); /* global var used due to differences between RPi board revisions */
   setup_io();
   verbose_mode=0;
   while ((opt = getopt(argc, argv, "dhmst:uvw:")) != -1) {
       switch (opt) {
	       case 't': /* set rtc's date and time from value following */
                   if (!set_rtc_time(stringio_to_epoch(optarg))) 
                     return 0;
		   break;
	       case 'w': /* set rtc's sram from value following */
                   ds1302_write_sram(optarg);
		   break;
	       case 'd': /* show the status of the rtc */
		   print_rtc_status();
		   break;
	       case 's': /* set system date and time from rtc */
                   set_system_time_from_rtc();
		   break;
	       case 'u': /* sync the oldest clock with the most recent */
                   sync_oldest_clock_to_newest();
		   break;
	       case 'v': /* verbose mode to display the different actions */
                   verbose_mode=1;
		   break;
	       case 'm': /* output the sram content as hexadecimal string */
                   ds1302_read_sram();
		   for (i=0; i<31; i++) {
		     printf("%.2X",ds1302_sram_copy[i]);
		   }
                   printf("\n");
		   break;
	       case 'h': /* help */
	       default: /* '?' */
		   fprintf(stderr, "Usage: %s\n", argv[0]);
		   fprintf(stderr, " CCYYMMDDHHMMSS defines the rtc date and time\n"
		                   "-t CCYYMMDDHHMMSS defines the rtc date and time, just like above\n"
		                   "-d display rtc status date, time and sram\n"
                                   "-s set the system date and time from rtc \n"
                                   "-u updates rtc clock if it is older than system clock, or updates rtc clock if it is older than rtc clock\n"
                                   "-w 0123456789ABCDEF0123456789ABCD 31 bytes long hexadecimal string to save in rtc's sram\n"
        	                   "-m returns the rtc sram content as a 31 bytes long hexadecimal string \n-v verbose display acknowledgement for actions\n"
				   "-h help\nSee for more subtle approaches http://www.tldp.org/HOWTO/Clock-2.html\n");
		   print_rtc_status();
		   exit(-1);
	       }
   }
   if (argc==2) {
     if (!set_rtc_time(stringio_to_epoch(argv[1]))) 
       return 0;
   }
   else
     if (argc==1) {
       set_system_time_from_rtc();
       return 0;
     }
   return 0;
}


